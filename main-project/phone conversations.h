#ifndef phone_conversations_H
#define phone_conversations_H

#include "constants.h"

struct date
{
    int day;
    int month;
    int year;
};
struct tarif
{
char tarif[MAX_STRING_SIZE];
};

struct nomer
{
    char nomer[MAX_STRING_SIZE];
};
struct time
{
    int hour;
    int minute;
    int second;
};
struct cost
{
    char cost [MAX_STRING_SIZE] ;
};

#endif
